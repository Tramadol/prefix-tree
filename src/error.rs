use std::fmt;

#[derive(Debug)]
pub struct DuplicateTrieKeyError {
	pub message: String
}