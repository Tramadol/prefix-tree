mod error;

use error::DuplicateTrieKeyError;
use std::{hash::Hash, collections::HashMap};

#[derive(Debug)]
pub struct Trie<K>
	where
		K: Eq + Hash + Clone
{
	end: bool,
	children: HashMap<K, Trie<K>>,
}

impl<K> Trie<K>
	where
		K: Eq + Hash + Clone
{
	fn new() -> Self {
		Trie {
			end: false,
			children: HashMap::new(),
		}
	}

	fn insert(&mut self, path: Vec<K>) -> Result<(), DuplicateTrieKeyError> {
		if path.is_empty() {
			return match self.end {
				true => {
					Err(DuplicateTrieKeyError{
						message: String::from("Key already exists!")
					})
				},
				false => {
					self.end = true;
					Ok(())
				}
			}
		}

		self.children
			.entry(path[0].clone())
			.or_insert(Trie::new())
			.insert(path[1..].to_vec())
	}

	fn fetch(&self, path: Vec<K>) -> bool {
		match path.len() {
			0 => self.end,
			_ => self.children
					.get(&path[0]).unwrap()
					.fetch(path[1..].to_vec())
		}
	}
}

#[cfg(test)]
mod tests {
	use crate::Trie;

	#[test]
	fn init_test() {
		let mut trie: Trie<char> = Trie::new();
		trie.insert("car".chars().collect());
		trie.insert("ca".chars().collect());
		trie.insert("ca".chars().collect());
		println!("{:?}", trie);

		let test = trie.fetch("ca".chars().collect());
		let test2 = trie.fetch("c".chars().collect());
		assert_eq!(test, true);
		assert_eq!(test2, false);
	}
}